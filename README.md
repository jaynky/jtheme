# Jtheme
A developer friendly, fast,  and modular Jekyll template. Easily mix and match styles and themes with easy to read code. 

## Installation
1. Either use Linux or Window's Ubuntu subsystem. 
2. Install Jekyll [following these directions](https://jekyllrb.com/)
3. Clone this repo with git
  `git clone git@gitlab.com:jaynky/thallia-tree-4-realz.git`.
4. launch in terminal with `bundle exec jekyll serve`
5. Visit [ http://127.0.0.1:4000/]( http://127.0.0.1:4000/)

## Usage
Jtheme has three main sections:

1. Includes
2. Layouts
3. Posts
### Includes
Jtheme's largest strength lies in its includes. Almost everything is an include. This means you can quickly mix and match to create the site you want. 


### Layouts
The includes are used in the layouts to build the page. In fact most of the layouts are nothing but collections of includes. 

### Posts 
Make all new posts in the _posts folder.
The title should be in a specific format.
`yyyy-mm-dd-title.md`.
Furthermore the front matter has to be placed at the top of every post. Here is an example:
```
---
layout: post
title: "{title}"
date: {date}
author: {author}
categories: {categories}
description: {description}
comments: true
excerpt_separator: <!--more-->
---
```
Replace all the text in the brackets "{}" with your own values. Lastly, place `<!--more-->` wherever you want the post to end on the blog page.


## contact
If you have any questions contact me through discord at Jay#3130. If you want to email me (expect  a longer delay) my email is jaynkystudio@gmx.com 
