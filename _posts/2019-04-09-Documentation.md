---
layout: post
title:  "Documentation"
date:   April 16, 2019
author: Jay
categories: jekyll update
description: Jtheme documentation #for meta description
comments: true
excerpt_separator: <!--more-->
---

<img id="viewport" src="/images/laptop.jpg" alt="laptop" class="col-md-12 lozad">


Now that you have an overview, lets dig into the details.
We will cover partials, config files, css, and html conventions. 
<!--more-->

#### Partials
Any reusable code is placed in a partial under the ```_includes``` folder. In addition to 
plain HTML, most of the libraries used are indeed partials. This means that the libraries are 
loaded within a single file. This means no requests, no round trips, and no outside internet.

#### Config Files
The site contains two config files. There is the basic ```_config.yml``` file that containts global site variables such as:

+  -```title```: for the ```<title>``` of the website
+  -```description```: for both the ```<meta>``` and RSS feed description
+  -```url```: Your domain name

In addition to the main config file, there is a separate file (widgets.yml) in ```/_data```
that contains more specialized variables such as your analytics code and disqus name.

#### CSS
All the custom CSS elements are in ```Jstyle.css``` under ```_includes```.
The file has a very detailed layout in order from global to more local elements and then from left to right starting at the navbar. One more thing, all the colors are in their own section. This means when you want to change the color scheme, all you have to do is edit a few declarations and blamo.

#### HTML 
Around 80% of all selectors in the theme's HTML are bootstrap. For those of you who might not be as familiar with bootstrap (which you should totally get acquainted with, its fantastic) here are a few conventions I used.
If you see:

```<div class="d-flex p-4 bg-transparent"></div>``` This simply helps position other elements vertically. Think of it like a glorified indent.

```<div class="col-md-12 d-flex justify-content-center">``` Instead of positioning vertically like the previous block of code, this div centers elements horizontally. 


#### Layouts
Here is where I journey away from convention. Normally the layouts are just standard templates that you customize in individual page markdwon files. Instead, each layout acts like its own page, and the markdown files is where you add the liquid variables. This makes the site even more modular as you can mix and match entire layouts. 


